const smallPizzaSize = document.querySelector('#small'),
    mediumPizzaSize = document.querySelector('#medium'),
    largePizzaSize = document.querySelector('#large'),
    pizzaSizeSelectors = document.querySelectorAll('input[name=pizza-size]'),
    creamPizzaBasis = document.querySelector('#cream'),
    tomatoPizzaBasis = document.querySelector('#tomato'),
    littleCheese = document.querySelector('#bit'),
    moreCheese = document.querySelector('#moreCheese'),
    lottaCheese = document.querySelector('#lottaCheese'),
    chickenTopping = document.querySelector('#chicken'),
    pepperoniTopping = document.querySelector('#pepperoni'),
    pineappleTopping = document.querySelector('#pineapple'),
    onionsTopping = document.querySelector('#onions'),
    baconTopping = document.querySelector('#bacon'),
    mushroomsTopping = document.querySelector('#mushrooms'),
    totalCost = document.querySelector('.total'),
    pizzaImg = document.querySelector('.pizza-img'),
    pizzaToppings = document.querySelectorAll('.pizza-topping'),
    mushrooms = document.querySelector('.mushrooms'),
    littleCheeseImg = document.querySelector('.little'),
    moreCheeseImg = document.querySelector('.moreCheese'),
    evenMoreCheeseImg = document.querySelector('.evenMoreCheese'),
    chickenImg = document.querySelector('.chicken-img'),
    pepperoniImg = document.querySelector('.pepperoni-img'),
    pineappleImg = document.querySelector('.pineapple-img'),
    onionsImg = document.querySelector('.onions-img'),
    baconImg = document.querySelector('.bacon-img'),
    cost = document.querySelector('.cost'),
    discount = document.querySelector('.discount'),
    closeDiscount = document.querySelector('.close-discount'),
    changeThemeBtn = document.querySelector('.change-theme'),
    wrapper = document.querySelector('.wrapper');

let price = 100;        //базова ціна піцци
let change = 0;       // додаткова ціна за додаткові інгредієнти
smallPizzaSize.addEventListener('change', changePizzaSize);
mediumPizzaSize.addEventListener('change', changePizzaSize);
largePizzaSize.addEventListener('change', changePizzaSize);

function changePizzaSize() {
    function clearPrevStyles() {    //щоб видалити стилі з самого блоку піцци і картинки грибів(картинка грибів відрізняється по розміру)
        const elements = [pizzaImg, mushrooms];
        const sizes = ['small', 'medium', 'large'];
        for (const el of elements) {
            for (const size of sizes) {
                if (el.classList.contains(`pizza-${size}`) || el.classList.contains(`mushrooms-${size}`)) {
                    el.classList.remove(`pizza-${size}`, `mushrooms-${size}`);
                }
            }
        }
        for (const pizzaTopping of pizzaToppings) {
            for (const size of sizes) {
                if (pizzaTopping.classList.contains(`pizza-${size}`)) {
                    pizzaTopping.classList.remove(`pizza-${size}`);
                }
            }

        }
    }
    clearPrevStyles();  
    if (this.id == 'small') {
        pizzaImg.classList.add('pizza-small'); //це зміна стилю самого блоку піци
        pizzaToppings.forEach(el => {       //зміна стилю інгредієнтів
            el.classList.add('pizza-small');
        })    
        mushrooms.classList.add('mushrooms-small'); //зміна стилю грибів
        price = 80;  //базова ціна
    }
    if (this.id == 'medium') {
        pizzaImg.classList.add('pizza-medium');
        pizzaToppings.forEach(el => {
            el.classList.add('pizza-medium');
        });
        mushrooms.classList.add('mushrooms-medium');
        price = 100;
    }
    if (this.id == 'large') {
        pizzaImg.classList.add('pizza-large');
        pizzaToppings.forEach(el => {
            el.classList.add('pizza-large');
        });
        mushrooms.classList.add('mushrooms-large');
        price = 120;
    }
    updatePrice(change); //щоб відобразити зміну ціни на сторінці
}

creamPizzaBasis.addEventListener('change', changePizzaTypeCream);

function changePizzaTypeCream() {
    if (pizzaImg.classList.contains('pizza-img-tomato')) {
        pizzaImg.classList.remove('pizza-img-tomato');
    };
}

tomatoPizzaBasis.addEventListener('change', changePizzaTypeTomato);

function changePizzaTypeTomato() {
    pizzaImg.classList.add('pizza-img-tomato');
}

littleCheese.addEventListener('change', changeCheeseAmount);
moreCheese.addEventListener('change', changeCheeseAmount);
lottaCheese.addEventListener('change', changeCheeseAmount);

function changeCheeseAmount() {
    if (!littleCheeseImg.classList.contains('hidden')) {
        littleCheeseImg.classList.add('hidden');
        change -= 10;
    }
    if (!moreCheeseImg.classList.contains('hidden')) {
        moreCheeseImg.classList.add('hidden');
        change -= 20;         //якщо був вибраний варіант з іншою кількістю сиру,щоб забрати стилі і додаткову націнку
    }
    if (!evenMoreCheeseImg.classList.contains('hidden')) {
        evenMoreCheeseImg.classList.add('hidden');
        change -= 30;
    }
    if (this.id === 'bit') {
        littleCheeseImg.classList.remove('hidden');
        change += 10;           //збільшення ціни піцци при додаванні сиру
    }
    if (this.id === 'moreCheese') {
        moreCheeseImg.classList.remove('hidden');
        change += 20;
    }
    if (this.id === 'lottaCheese') {
        evenMoreCheeseImg.classList.remove('hidden');
        change += 30;
    }
    updatePrice(change);
}

const toppings = [chickenTopping, pepperoniTopping, pineappleTopping, onionsTopping, baconTopping, mushroomsTopping];
toppings.forEach(topping => {
    topping.addEventListener('change', addTopping);
});

function showImg(img) {
    img.classList.toggle('hidden')
}
function addTopping() {
    switch (this.id) {
        case 'chicken':               //якщо id вибраного інпуту
            showImg(chickenImg);
            if (chickenImg.classList.contains('hidden')) {
                change -= 10;
            } else {
                change += 10;
            }
            break;
        case 'pepperoni':
            showImg(pepperoniImg);
            if (pepperoniImg.classList.contains('hidden')) {
                change -= 5;
            } else {
                change += 5;
            }
            break;
        case 'pineapple':
            showImg(pineappleImg);
            if (pineappleImg.classList.contains('hidden')) {
                change -= 5;
            } else {
                change += 5;
            }
            break;
        case 'onions':
            showImg(onionsImg);
            if (onionsImg.classList.contains('hidden')) {
                change -= 3;
            } else {
                change += 3;
            }
            break;
        case 'bacon':
            showImg(baconImg);
            if (baconImg.classList.contains('hidden')) {
                change -= 5;
            } else {
                change += 5;
            }
            break;
        case 'mushrooms':
            showImg(mushrooms);
            if (mushrooms.classList.contains('hidden')) {
                change -= 3;
            } else {
                change += 3;
            }
            break;
        default:
            break;
    }
    updatePrice(change);
}

function updatePrice(change) {                    //для відображення ціни
    cost.innerHTML = price + change + ' UAH';
}
function showDiscount() {
    discount.style.display = 'flex';
}

setTimeout(showDiscount, 3000);

closeDiscount.addEventListener('click', () => {
    discount.style.display = 'none';
});

changeThemeBtn.addEventListener('click', changeTheme);

function changeTheme() {
    wrapper.classList.toggle('day-theme');
    changeThemeBtn.classList.toggle('day-theme');
}