const ball = document.querySelector('.ball');
const firstGate = document.querySelector('.football-gate-first');
const secondGate = document.querySelector('.football-gate-second');
const scoreBoard = document.querySelector('.scoreboard');
let firstGateGoals = 0;
let secondGateGoals = 0;


firstGate.addEventListener('dragover', allowDrow);
secondGate.addEventListener('dragover', allowDrow);

function allowDrow(e){
  e.preventDefault();
}

firstGate.addEventListener('drop', scoreGoal);
secondGate.addEventListener('drop', scoreGoal);


function scoreGoal(e){
  ball.style.left = (e.clientX - ball.width/2) +'px';
	ball.style.top = (e.clientY - ball.height/2) + 'px'; 
  if(this == firstGate){
  firstGateGoals++;            //якщо подія дропу відбулась в зоні елементу firstGate
  alert('First teams scored!'); //то значення кількості голів протилежної команди збільшується
  }
  if(this == secondGate){
  secondGateGoals++;  
  alert('Second team scored');
  }
  scoreBoard.innerHTML = `Score is ${firstGateGoals}:${secondGateGoals}` //Зміна результату на таблі
  setTimeout( function(){  //Через пів секунди м'яч повертається на середину поля
    ball.style.left = '50%'; 
    ball.style.top = '30%';
  }, 600);
  
}