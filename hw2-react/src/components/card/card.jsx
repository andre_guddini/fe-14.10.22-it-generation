import React from "react";
import "./card.css";
import CardInfo from "../card-info/card-info";

const Card = ({data}) => {    
    return (
        <div className="cards">
        {
            Array.isArray(data)? data.map((e,i)=>{
                return <CardInfo info={e} key={i}></CardInfo>
            }) :null
        }
        </div>
    )
}

export default Card;