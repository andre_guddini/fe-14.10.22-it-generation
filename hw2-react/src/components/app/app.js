import React from "react";
import { Title } from "../title/title";
import {results} from "../../data/data";
import Card from "../card/card";


function App(){
    return(
        <>
        <Title></Title>
        <Card data={results}></Card>
        </>
    )
}


export default App;