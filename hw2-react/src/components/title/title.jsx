import React from "react"
import "./title.css"

export const Title = () =>{
    return (
        <h1 className="title">The Star Wars Card Collection</h1>
    )
}