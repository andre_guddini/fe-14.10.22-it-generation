import React from "react";
import "./card-info.css"

function CardInfo({info}){
    const {birth_year,eye_color,gender,hair_color,height,mass,name,skin_color,url} = info;

    return(
       <div className="card">
       <div className="name"><h2>{name}</h2></div>
       <div className="birth">Birth: {birth_year}</div>
       <div className="gender">Gender: {gender}</div>
       <div className="eye-color">Eye color: {eye_color}</div>
       <div className="hair-color">Hair color: {hair_color}</div>
       <div className="height">Height: {height}</div>
       <div className="mass">Mass: {mass}</div>
       <div className="skin-color">Skin color: {skin_color}</div>
       <div className="url"><a href={url} target='_blank'>Hero Link</a></div>
       </div>
    )
        
    
}

export default CardInfo;